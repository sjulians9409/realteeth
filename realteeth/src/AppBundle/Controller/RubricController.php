<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 5/02/18
 * Time: 02:24 PM
 */

namespace AppBundle\Controller;


use AppBundle\Constants\ControllerConstants;
use AppBundle\Entity\Criterion;
use AppBundle\Entity\CriterionAnswer;
use AppBundle\Entity\PatientCase;
use AppBundle\Entity\Road;
use AppBundle\Entity\User;
use AppBundle\Entity\UserAnswer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Acl\Exception\Exception;

class RubricController extends Controller
{
    public static $PAIN_ROAD_TWIG = "rubric/painRoadRubric.html.twig";
    public static $CLINICAL_REVIEW = "rubric/clinicalReviewRubric.html.twig";
    public static $CLINICAL_TEST = "rubric/clinicalTestRubric.html.twig";
    public static $XRAY = "rubric/xRayRubric.html.twig";
    public static $FEEDBACK = "rubric/showFeedback.html.twig";


    public static $PAIN_CODE = ControllerConstants::PAIN_CODE;
    public static $CLINICAL_REVIEW_CODE = ControllerConstants::CLINICAL_REVIEW_CODE;
    public static $CLINICAL_TEST_CODE = ControllerConstants::CLINICAL_TEST_CODE;
    public static $XRAY_CODE = ControllerConstants::XRAY_CODE;
    public static $DIAGNOSIS_CODE = ControllerConstants::DIAG_TEST_CODE;
    public static $FALSE_CODE = 0;

    public function showRubricAction(Request $request){


        $requestedCode = $request->query->get('rubric',null);
        $idCase = $request->query->get('idCase',null);
        $teethQuantity = $request->query->get('teethQuantity',null);
        $teethQuantity = $teethQuantity -1;
        $requestRubric = $this->getRubricByCode($requestedCode);
        $criteriaArray = $this->getCritireafromRoad($requestedCode);
        return $this->render($requestRubric,array('idCase' => $idCase, 'criteriaArray' => $criteriaArray, 'teethQuantity' => $teethQuantity));
    }

    public function getRubricByCode($code){
        if($code == $this::$PAIN_CODE){
            return $this::$PAIN_ROAD_TWIG;
        }else if($code == $this::$CLINICAL_REVIEW_CODE){
            return $this::$CLINICAL_REVIEW;
        }else if($code == $this::$CLINICAL_TEST_CODE){
            return $this::$CLINICAL_TEST;
        }else if($code == $this::$XRAY_CODE){
            return $this::$XRAY;
        }
        throw new NotFoundHttpException("No encontramos la pagina con codigo $code");
    }

    public function getCritireafromRoad($roadCode){
        /** @var Road $roadEntity */
        $roadEntity = $this->getDoctrine()->getRepository('AppBundle:Road')->findOneBy(array('code' => $roadCode));
        if($roadEntity == null){
            return false;
        }
        return $roadEntity->getIdAspect()->getCriteria()->toArray();
    }

    public function registerRubricUserAnswersAjaxAction(Request $request){

        $response = true;
        $criterionAnswersJSON = $request->query->get('jsonCriteria',null);
        $criterionAnswersArray = json_decode($criterionAnswersJSON);

        if(count($criterionAnswersArray) <= 0){
            return new Response(json_encode(true));
        }

        $patientCaseEntity = $this->getDoctrine()->getRepository('AppBundle:PatientCase')
                                                ->find($criterionAnswersArray[0]->idPatientCase);
        foreach ($criterionAnswersArray as $answer){
            $criterionQuestionEntity = $this->getDoctrine()->getRepository('AppBundle:CriterionQuestion')
                                                        ->find($answer->idCriterionQuestion);
            if($criterionQuestionEntity == null){
                $response = false;
            }
            $response = $this->getDoctrine()->getRepository('AppBundle:UserAnswer')
                            ->createRegister($this->getUser(),$patientCaseEntity,$criterionQuestionEntity,$answer->answer);
            if($response != true ){
                return new Response(json_encode($response));
            }
        }

        return new Response(json_encode($response));

    }

    /**
     * This function returns UserAnswers
     * @param $idUser
     * @param $idCriterionQuestion
     * @param $idPatientCase
     * @return array[UserAnswer] => userAnswers
     */
    public function getUserAnswersByCriterionQuestion($idUser, $idCriterionQuestion, $idPatientCase){
        $userAnswer = $this->getDoctrine()
                            ->getRepository('AppBundle:UserAnswer')
                            ->findBy(array('idUser' => $idUser,
                                            'idCriterionQuestion' => $idCriterionQuestion,
                                            'idPatientCase' => $idPatientCase));
        return $userAnswer;
    }

    /**
     * This function returns CriterionAnswers
     * @param $idCriterionQuestion
     * @param $idPatientCase
     * @return array[CriterionAnswer] => CriterionAnswers
     */
    public function getAnswersByCriterionQuestion($idCriterionQuestion, $idPatientCase){
        $criterionAnswer = $this->getDoctrine()
                                ->getRepository('AppBundle:CriterionAnswer')
                                ->findBy(array('idCriterionQuestion' => $idCriterionQuestion,
                                                'idPatientCase' => $idPatientCase));
        return $criterionAnswer;
    }


    /**
     * @param $userAnswer
     * @param $criterionAnswer
     * @return bool
     */
    public function checkUserAnswersVsCriterionAnswers($userAnswers, $criterionAnswers){

        if(count($userAnswers) < count($criterionAnswers)){
            return false;
        }

        if(count($userAnswers) == 0 && count($criterionAnswers) == 0 ){
            return true;
        }

        /** @var UserAnswer $userAnswer */
        foreach ($userAnswers as $userAnswer){
            $userAnswerExist = false;
            /** @var CriterionAnswer $criterionAnswer */
            foreach ($criterionAnswers  as $criterionAnswer){
                if($userAnswer->getDetail() == $criterionAnswer->getDetail()){
                    $userAnswerExist = true;
                }
            }
            if($userAnswerExist == false){
                return false;
            }
        }

        return true;
    }

    /**
     * @param $idCriterion
     * @return null|array
     */
    public function getCriterionQuestions($idCriterion){
        $criterionQuestions = $this->getDoctrine()
                                    ->getRepository('AppBundle:CriterionQuestion')
                                    ->findBy(array('idCriterion' => $idCriterion));
        return $criterionQuestions;
    }

    /**
     * This function vrifies if a student has aproved a criterion
     * @param $idCriterion
     * @param $idUser
     * @param $idPatientCase
     * @return bool
     */
    public function evaluateCriterion($idCriterion, $idUser, $idPatientCase){
        $criterionQuestions = $this->getCriterionQuestions($idCriterion);
        foreach ($criterionQuestions as $criterionQuestion){
            $userAnswers = $this->getUserAnswersByCriterionQuestion($idUser,$criterionQuestion, $idPatientCase);
            $criterionAnswers = $this->getAnswersByCriterionQuestion($criterionQuestion,$idPatientCase);
            $checkAnswers = $this->checkUserAnswersVsCriterionAnswers($userAnswers,$criterionAnswers);
            if($checkAnswers == false){
                return false;
            }
        }
        return true;
    }

    /**
     * This function gets all criteria
     * @return array
     */
    public function getAllCriteria(){
        $criteria = $this->getDoctrine()
                        ->getRepository('AppBundle:Criterion')
                        ->findAll();
        return $criteria;
    }

    /**
     * This ufnction returns a map [bool|Criterion]
     * @param $idUser
     * @param $idPatientCase
     * @return array => map [bool|Criterion]
     */
    public function showCriteriaResult($idUser,$idPatientCase){
        $criteria = $this->getAllCriteria();
        $criteriaResult = array();

        foreach ($criteria as $criterion){
            $criterionResult = $this->evaluateCriterion($criterion,$idUser,$idPatientCase);
            $criteriaResult[$criterionResult][] = $criterion;
        }
        return $criteriaResult;
    }

    /**
     * @param $criteriaResultMap [bool|Criterion]
     * @return int
     */
    public function getUserPoints($criteriaResultMap){
        $userPoints = 0;
        $aprovedCriteria = $criteriaResultMap[true];
        /** @var Criterion $criterion */
        foreach ($aprovedCriteria as $criterion){
            $userPoints += $criterion->getMaxPoints();

        }
        return $userPoints;
    }

    /**
     *
     * @param $criteriaResultMap
     * @return array|bool
     */
    public function getFeedBack($criteriaResultMap){
        if(array_key_exists($this::$FALSE_CODE, $criteriaResultMap) == false) {
            return false;
        }
        $failedCriteria = $criteriaResultMap[false];

        $feedbackArray = array();
        $aspects = array();
        /** @var Criterion $failedCriterion */
        foreach ($failedCriteria as $failedCriterion){
            $aspectName = $failedCriterion->getIdAspect()->getName();
            if(in_array($aspectName, $aspects) == false){
                $aspects[]=$aspectName;
            }
            $feedbackArray[$failedCriterion->getIdAspect()->getName()][] =
                $failedCriterion->getCriterionFeedback()->getDetail();
        }
        $feedbackArray['aspects'] = $aspects;
        return $feedbackArray;
    }


    public function showFeedbackAction(Request $request){
        $idCase = $request->query->get('idCase',null);

        $criteriaResult = $this->showCriteriaResult($this->getUser(),$idCase);
        $userPoints = $this->getUserPoints($criteriaResult);
        $feedback = $this->getFeedBack($criteriaResult);
        $this->calculateStudentLevel($this->getUser(),$criteriaResult,$idCase);

        return $this->render($this::$FEEDBACK, array('userPoints' => $userPoints, 'feedback' => $feedback));
    }

    /**
     * @param User $userEntity
     * @param $criteriaResult
     * @return bool
     */
    public function checkIfLevelDown(User $userEntity,$criteriaResult){
        if(array_key_exists($this::$FALSE_CODE, $criteriaResult) == false) {
            return false;
        }
        $failedCriteria = $criteriaResult[false];
        $lostPoints = 0;
        /** @var Criterion $criterion */
        foreach ($failedCriteria as $criterion){
            $lostPoints += $criterion->getMaxPoints();
        }

        $lostDiagnosisCriteria = $this->checkIfHasLostDiagnosisCriteria($failedCriteria);

        if($lostPoints > ControllerConstants::MAX_LOST_POINTS || $lostDiagnosisCriteria == true){
            $this->getDoctrine()
                ->getRepository('AppBundle:User')
                ->loseConsecutiveRegisters($userEntity);
            return true;
        }
        return false;
    }

    /**
     * @param $failedCriteria
     * @return bool
     */
    public function checkIfHasLostDiagnosisCriteria($failedCriteria){
        $pulparCriterion = $this->getDoctrine()
                                ->getRepository('AppBundle:Criterion')
                                ->find(ControllerConstants::ID_PULPAR_DIAGNOSIS);
        $periapicalCriterion =  $this->getDoctrine()
                                    ->getRepository('AppBundle:Criterion')
                                    ->find(ControllerConstants::ID_PERIAPICAL_DIAGNOSIS);
        if(in_array($pulparCriterion,$failedCriteria) == true ||
            in_array($periapicalCriterion,$failedCriteria) == true){
            return true;
        }
        return false;
    }


    /**
     * @param User $userEntity
     * @return bool
     */
    public function checkIfLevelUp(User $userEntity){
        $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->addConsecutiveCase($userEntity);
        $consecutiveCases = $userEntity->getConsecutiveCases();
        if($consecutiveCases >= ControllerConstants::CONSECUTIVE_CASES){
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param $approve
     * @return bool
     */
    public function setStudentLevel(User $user ,$approve){
        /** @var Level $userLevel */
        $userLevel = $this->getDoctrine()->getRepository('AppBundle:UserByLevel')->getUserLevel($user->getId());
        $userLevelOrder = $userLevel->getOrder();
        $maxLevel = ControllerConstants::MAX_LEVEL;
        $minLevel = ControllerConstants::MIN_LEVEL;

        if($approve == true){
            if($userLevelOrder+1 <= $maxLevel){
                $level = $this->getDoctrine()->getRepository('AppBundle:Level')->findOneBy(array('order' => $userLevelOrder+1));
                /** @var UserByLevel $userByLevel */
                $userByLevel = $this->getDoctrine()->getRepository('AppBundle:UserByLevel')->findOneBy(array('idUser' => $user->getId()));
                $userByLevel->setIdLevel($level);
                $this->getDoctrine()->getEntityManager()->persist($userByLevel);
                $this->getDoctrine()->getEntityManager()->flush();
                return true;
            }
        }else{
            if($userLevelOrder-1 >= $minLevel){
                $level = $this->getDoctrine()->getRepository('AppBundle:Level')->findOneBy(array('order' => $userLevelOrder-1));
                /** @var UserByLevel $userByLevel */
                $userByLevel = $this->getDoctrine()->getRepository('AppBundle:UserByLevel')->findOneBy(array('idUser' => $user->getId()));
                $userByLevel->setIdLevel($level);
                $this->getDoctrine()->getEntityManager()->persist($userByLevel);
                $this->getDoctrine()->getEntityManager()->flush();
                return true;
            }
        }
        return false;
    }

    /**
     * @param User $user
     * @param $criteriaResult
     * @param $idCase
     * @return bool|Response
     */
    public function calculateStudentLevel(User $user, $criteriaResult,$idCase){
        try {
            $succeed = true;

            $goesDown = $this->checkIfLevelDown($user, $criteriaResult);
            if ($goesDown == true) {
                $succeed = false;
                $this->createAssignedCaseRegister($user, $idCase, $succeed);
                $result = $this->setStudentLevel($user, false);
                return $result;
            }

            $this->createAssignedCaseRegister($user, $idCase, $succeed);

            $goesUp = $this->checkIfLevelUp($user);
            if ($goesUp == true) {
                $this->resetConsecutiveCases($user);
                $result = $this->setStudentLevel($user, true);
                return $result;
            }
            return true;
        }catch (\Exception $e){
            $response = new Response();
            $response->setStatusCode(400);
            $response->getContent($e->getMessage());
            return $response;
        }
    }

    public function resetConsecutiveCases($idUser){
        return $this->getDoctrine()
                    ->getRepository('AppBundle:User')
                    ->loseConsecutiveRegisters($idUser);
    }

    public function createAssignedCaseRegister($idUser, $idCase, $succeed){
        try {
            $this->getDoctrine()->getRepository('AppBundle:AssignedCases')->createNewAssignedCase($idUser, $idCase, $succeed);
        }catch (\Exception $e){
            throw $e;
        }
    }
}