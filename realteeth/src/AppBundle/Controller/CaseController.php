<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 7/10/17
 * Time: 12:00 PM
 */

namespace AppBundle\Controller;

use AppBundle\Constants\ControllerConstants;
use AppBundle\Entity\Answer;
use AppBundle\Entity\AnswerByImage;
use AppBundle\Entity\AssignedCases;
use AppBundle\Entity\CaseByRoad;
use AppBundle\Entity\PatientCase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CaseController extends Controller
{
    public function showRoadAction(Request $request){
        $idCase = $request->query->get('idcase',null);
        $code = $request->query->get('code',null);

        $twig = $this->getTwigByCode($code);
        $questions = $this->getQuestionsByRoad($code);
        $twigParameters = $this->getTwigParameters($questions,$idCase,$code,$request);

        return $this->render($twig,$twigParameters);
    }

    public function getTwigByCode($code){
        $twig = null;
        if($code == ControllerConstants::PAIN_CODE){
            $twig = ControllerConstants::PAIN_ROAD;
        }
        else if($code == ControllerConstants::PRE_CLINICAL_REVIEW_CODE){
            $twig = ControllerConstants::PRE_CLINICAL_REVIEW_ROAD;
        }
        else if($code == ControllerConstants::CLINICAL_REVIEW_CODE){
            $twig = ControllerConstants::CLINICAL_REVIEW_ROAD;
        }
        else if($code == ControllerConstants::PRE_CLINICAL_TESTS_CODE){
            $twig = ControllerConstants::PRE_CLINICAL_TESTS_ROAD;
        }
        else if($code == ControllerConstants::PERIAPICAL_TEST_CODE){
            $twig = ControllerConstants::PERIAPICAL_TEST_ROAD;
        }
        else if($code == ControllerConstants::SENSE_TEST_CODE){
            $twig = ControllerConstants::SENSE_TEST_ROAD;
        }
        else if($code == ControllerConstants::XRAY_CODE){
            $twig = ControllerConstants::XRAY_TEST_ROAD;
        }
        else if($code == ControllerConstants::DIAG_TEST_CODE){
            $twig = ControllerConstants::DIAGNOSIS_RUBRIC;
        }else if($code == ControllerConstants::PROGRESS_CODE){
            $twig = ControllerConstants::SHOW_PROGRESS;
        }
        if($twig == null){
            throw $this->createNotFoundException('There is no page to show with code "'.$code.'"');
        }

        return $twig;
    }

    public function getQuestionsByRoad($code){
        $road = $this->getDoctrine()->getRepository('AppBundle:Road')->findOneBy(array('code' => $code));
        $questions = false;
        if($road != null) {
            $questions = $this->getDoctrine()->getRepository('AppBundle:Question')->findBy(array('idRoad' => $road->getId()));
        }
        return $questions;
    }

    public function getTwigParameters($questions,$idCase,$code,$request){
        $twigParameters = array('questions' => $questions, 'idCase' => $idCase);
        if($code == ControllerConstants::PRE_CLINICAL_REVIEW_CODE){
            $twigParameters['detail'] = $this->getRoadDetail($idCase,ControllerConstants::CLINICAL_REVIEW_CODE);
            $imageNames =
                $this->getDoctrine()->getRepository('AppBundle:ImgByRouteCase')
                    ->getImagesByCase($idCase,ControllerConstants::CLINICAL_REVIEW_CODE);
            if($imageNames != false){
                $twigParameters['images'] = $imageNames;
            }
        }
        else if($code == ControllerConstants::SENSE_TEST_CODE){

            $twigParameters['domainTeethArray'] = $this->getDoctrine()
                                                        ->getRepository('AppBundle:CaseByRoad')
                                                        ->getDomainTeethArrayFromCase($idCase);

            $twigParameters['detail'] = $this->getRoadDetail($idCase,ControllerConstants::SENSE_TEST_CODE);
        }
        else if($code == ControllerConstants::XRAY_CODE){
            $twigParameters[ControllerConstants::CORONA_CLINICA] =
                $this->getDoctrine()->getRepository('AppBundle:StructureByQuestion')
                                    ->getQuestionsByStructure(ControllerConstants::CORONA_CLINICA);

            $twigParameters[ControllerConstants::RAIZ] =
                $this->getDoctrine()->getRepository('AppBundle:StructureByQuestion')
                    ->getQuestionsByStructure(ControllerConstants::RAIZ);

            $twigParameters[ControllerConstants::TEJIDOS_SOPORTE] =
                $this->getDoctrine()->getRepository('AppBundle:StructureByQuestion')
                    ->getQuestionsByStructure(ControllerConstants::TEJIDOS_SOPORTE);
            $imageNames =
                $this->getDoctrine()->getRepository('AppBundle:ImgByRouteCase')
                    ->getImagesByCase($idCase,ControllerConstants::XRAY_CODE);
            if($imageNames != false){
                $twigParameters['images'] = $imageNames;
            }


        }
        else if($code == ControllerConstants::DIAG_TEST_CODE){
            $teethQuantity = $request->query->get('teethQuantity',null);
            $twigParameters['teethQuantity'] = $teethQuantity -1;
            $twigParameters['idCase'] = $request->query->get('idCase',null);
        }

        return $twigParameters;
    }


    public function getRoadDetail($idCase, $roadCode){
        $road = $this->getDoctrine()
                    ->getRepository('AppBundle:Road')
                    ->findOneBy(array('code' => $roadCode));
        /** @var CaseByRoad $caseByRoad */
        $caseByRoad = $this->getDoctrine()
                    ->getRepository('AppBundle:CaseByRoad')
                    ->findOneBy(array('idPatientCase' => $idCase,
                        'idRoad'=>$road));
        return $caseByRoad->getDetail();
    }

    public function getQuestionAnswerAction(Request $request){
        $idCase = $request->query->get('idCase',null);
        $idQuestion = $request->query->get('idQuestion',null);
        //dump($idCase);
        //dump($idQuestion);
        /** @var Answer $answer */
        $answer = $this->getDoctrine()->getRepository('AppBundle:Answer')->findOneBy(array('idQuestion' => $idQuestion, 'idPatientCase'=> $idCase));
        if($answer == null){
            return new Response(json_encode(false));
        }else{
            /** @var AnswerByImage $answerByImage */
            $answerByImage = $this->getDoctrine()->getRepository('AppBundle:AnswerByImage')->findOneBy(array('idAnswer' => $answer->getId()));
            $imgsrc = null;
            if($answerByImage != null){
                $imgsrc = $answerByImage->getIdImage()->getUrl();
            }
            return new Response(json_encode(array('detail' => $answer->getDetail(),'imgsrc' => $imgsrc)));
        }


    }

    public function verifyDiagnosisAction(Request $request){
        $periapical = $request->query->get('periapical', null) ;
        $pulpar = $request->query->get('pulpar',null);
        $idCase = $request->query->get('idcase');
        /** @var PatientCase $case */
        $case = $this->getDoctrine()->getRepository('AppBundle:PatientCase')->find($idCase);

        $verifPeriapical = false;
        $verifPulpar = false;
        if(strtolower($case->getIdDPeriapical()->getName()) == strtolower($periapical)){
            $verifPeriapical = true;
        }
        if(strtolower($case->getIdDPulpar()->getName()) == strtolower($pulpar)){
            $verifPulpar = true;
        }

        return new Response(json_encode(array('periapical' => $verifPeriapical, 'pulpar' => $verifPulpar)));
    }

    /**
     * @param $answer
     */
    public function getImgSrc($answer)
    {
        $answerByImage = $this->getDoctrine()->getRepository('AppBundle:AnswerByImage')->findOneBy(array('idAnswer' => $answer->getId()));
        $imgsrc = null;
        if ($answerByImage != null) {
            $imgsrc = $answerByImage->getIdImage()->getUrl();
        }
        return $imgsrc;
    }

    public function showPatientCasesProgressAction(Request $request){
        try {

            $firstTime = $request->query->get('first',null);
            if($firstTime != null){
                $this->resetAssignedCases();
            }

            $twig = $this->getTwigByCode(ControllerConstants::PROGRESS_CODE);

            $assignedCases = $this->getDoctrine()->getRepository('AppBundle:AssignedCases')->getAssignedCasesEnabled($this->getUser());
            $maxCases = ControllerConstants::MAX_CASES;
            $succeedCases = $this->countSucceedCases($assignedCases);
            $failedCases = count($assignedCases) - $succeedCases;

            $parameters =  array("maxCases" => $maxCases,
                                "assignedCases" => $assignedCases,
                                "succeedCases" => $succeedCases,
                                "failedCases" => $failedCases);
            
            if($firstTime != null) {
                $parameters['first'] = true;
            }

            if($failedCases >= ControllerConstants::MAX_FAILED_CASES ){
                $parameters['stop'] = true;
            }
            return $this->render($twig,$parameters);
        }catch (\Exception $e){
            $response = $this->createBadRequestResponse($e->getMessage());
            return $response;
        }
    }

    public function resetAssignedCases(){
        $this->getDoctrine()->getRepository('AppBundle:AssignedCases')->resetAssignedCases($this->getUser());
    }

    public function countSucceedCases($assignedCases){
        $succeedCases = 0;
        /** @var AssignedCases $assignedCase */
        foreach ($assignedCases as $assignedCase){
            if($assignedCase->getSucceed() == true){
                $succeedCases++;
            }
        }
        return $succeedCases;
    }

    public function createBadRequestResponse($message){
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_BAD_REQUEST)
            ->setData(array('Response' => "BAD REQUEST",
                'status' => Response::HTTP_BAD_REQUEST ,
                'message' => $message));
        return $response;
    }

    public function createOKRequestResponse($message){
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_OK)
            ->setData(array('Response' => $message, 'status' => Response::HTTP_OK ));
        return $response;
    }


}