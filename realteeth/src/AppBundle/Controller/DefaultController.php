<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }
    public function pruebasAction(Request $request){
        //$this->forward('AppBundle:Case:getQuestionAnswer',array('idCase' => 12,'idQuestion' => 1));

        return $this->redirectToRoute('AppBundle_get_question_answer',array('idCase' => 12,'idQuestion' => 1));
        die();
    }
}
