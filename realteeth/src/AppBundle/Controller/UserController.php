<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 21/09/17
 * Time: 03:08 PM
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class UserController extends Controller
{
    public function homeAction(Request $request){
        //Esto se debe hacer solo la primera vez que ingrese al home
        //Poner un parametro despues de hacer login
        $welcome = $request->query->get('welcome',null);
        if($welcome == 1) {
            if (isset($_SESSION['idCases']) == true) {
                unset($_SESSION["idCases"]);
            }
            //dump("primera vez");
        }

        return $this->render('fos_user/home_student.html.twig',array());
    }
}