<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 11/04/18
 * Time: 03:06 PM
 */

namespace AppBundle\Controller;

use AppBundle\Constants\ControllerConstants;
use AppBundle\Entity\CaseByRoad;
use Proxies\__CG__\AppBundle\Entity\PatientCase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    public function showAdminHomeAction(Request $request){
        return $this->render(ControllerConstants::ADMIN_HONE,array());
    }

    public function showCreateCaseAction(){
        return $this->render(ControllerConstants::ADMIN_CREATE_CASE,array());
    }

    public function addAnswerAction(Request $request){
        try {
            $jsonStringAnswers = $request->getContent();

            $jsonAnswers = json_decode($jsonStringAnswers);

            $result = $this->getDoctrine()
                            ->getRepository('AppBundle:Answer')
                            ->createAnswersFromJson($jsonAnswers);

            $response = new JsonResponse();
            $response->setStatusCode(Response::HTTP_OK)
                ->setData(array('Response' => $result, 'status' => Response::HTTP_OK ));

            return $response;
        }catch (\Exception $e){
            $response = new JsonResponse();
            $response->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setData(array('Response' => "BAD REQUEST",
                    'status' => Response::HTTP_BAD_REQUEST ,
                    'message' => $e->getMessage()));
            return $response;
        }
    }

    public function createCaseAndHistoryAction(Request $request){
        try {
            $jsonStringAnswers = $request->getContent();

            $jsonAnswers = json_decode($jsonStringAnswers);

            $history = "";
            $level = "";

            foreach($jsonAnswers as $jsonAnswer){
                if($jsonAnswer->idQuestion == "history"){
                    $history = $jsonAnswer->detail;
                }
                else if($jsonAnswer->idQuestion == "difficulty"){
                    $level = $jsonAnswer->detail;
                }
            }

            if($history == "" || $level == ""){
                $response = $this->createBadRequestResponse("Algo paso con los datos que agregaste.");
                return $response;
            }

            /** @var PatientCase $result */
            $result = $this->getDoctrine()
                ->getRepository('AppBundle:PatientCase')
                ->createPatientCase($history, $level);

            $response = $this->createOKRequestResponse($result->getId());
            return $response;

        }catch(\Exception $e){
            $response = $this->createBadRequestResponse($e->getMessage());
            return $response;
        }
    }

    public function createBadRequestResponse($message){
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_BAD_REQUEST)
            ->setData(array('Response' => "BAD REQUEST",
                'status' => Response::HTTP_BAD_REQUEST ,
                'message' => $message));
        return $response;
    }

    public function createOKRequestResponse($message){
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_OK)
            ->setData(array('Response' => $message, 'status' => Response::HTTP_OK ));
        return $response;
    }

    public function addCaseByRoadDescriptionAction(Request $request){
        try {
            $jsonStringAnswers = $request->getContent();
            $jsonAnswers = json_decode($jsonStringAnswers);

            /** @var CaseByRoad $caseByRoad */
            $caseByRoad = $this->getDoctrine()
                            ->getRepository('AppBundle:CaseByRoad')
                            ->createCaseByRoad($jsonAnswers->idCase, $jsonAnswers->roadCode, $jsonAnswers->description);

            $response = $this->createOKRequestResponse("CaseByRoad ID ".$caseByRoad->getId());
            return $response;

        }catch (\Exception $e){
            $response = $this->createBadRequestResponse($e->getMessage());
            return $response;
        }
    }

    public function showAnalysisByCaseAction(Request $request){
        try {

            $jsonString = $request->getContent();
            $json = json_decode($jsonString);
            $code = $json->code;
            $teethQuantity = $json->teethQuantity;

            $twig = $this->getAnalysisTwigByCode($code);

            return $this->render($twig,array("teethQuantity" => $teethQuantity-1 ));

        }catch (\Exception $e){
            $response = $this->createBadRequestResponse($e->getMessage());
            return $response;
        }
    }

    /**
     * @param $code
     * @return null|string
     * @throws \Exception
     */
    public function getAnalysisTwigByCode($code){
        $twig = null;
        if($code == ControllerConstants::PAIN_CODE){
            $twig = ControllerConstants::ANALYSIS_PAIN_TWIG;
        }elseif($code == ControllerConstants::CLINICAL_REVIEW_CODE){
            $twig = ControllerConstants::ANALYSIS_CLREVIEW_TWIG;
        }elseif($code == ControllerConstants::CLINICAL_TEST_CODE){
            $twig = ControllerConstants::ANALYSIS_CLTESTS_TWIG;
        }elseif($code == ControllerConstants::XRAY_CODE){
            $twig = ControllerConstants::ANALYSIS_XRAY_TWIG;
        }elseif($code == ControllerConstants::DIAG_TEST_CODE){
            $twig = ControllerConstants::ANALYSIS_DIAGNOSIS_TWIG;
        }

        if($twig==null){
            throw new \Exception("No existe el codigo ".$code);
        }

        return $twig;
    }

    public function registerCriterionAnswersAction(Request $request){
        try {
            $jsonString = $request->getContent();
            $json = json_decode($jsonString);

            $result = $this->getDoctrine()
                ->getRepository('AppBundle:CriterionAnswer')
                ->registerCriterionASnwersFromJson($json);

            $response = $this->createOKRequestResponse($result);
            return $response;

        }catch (\Exception $e){
            $response = $this->createBadRequestResponse($e->getMessage());
            return $response;
        }

    }

    public function testUploadFileTwigAction(Request $request){
        return $this->render("admin/testfile.html.twig");
    }

    public function testUploadFileAction(Request $request){

        $file = new File($_FILES["files"]["tmp_name"]);

        $file->move($this->getParameter('images_directory'),$_FILES["files"]["name"]);

        dump($_FILES);

        die();
    }

    public function showUploadImagesTwigAction(Request $request){
        $jsonString = $request->getContent();
        $json = json_decode($jsonString);
        $teethQuantity = $json->teethQuantity;
        $road = $json->road;

        return $this->render(ControllerConstants::ADMIN_UPLOAD_IMAGES, array("teethQuantity" => $teethQuantity - 1 , "road" => $road));
    }

    public function uploadImageAction(Request $request){
        try {
            $file = new File($_FILES["files"]["tmp_name"]);

            $file->move($this->getParameter('images_directory'),$_FILES["files"]["name"]);

            $imgName = $_FILES["files"]["name"];

            $response = $this->createOKRequestResponse($imgName);
            return $response;
        }catch (\Exception $e){
            $response = $this->createBadRequestResponse($e->getMessage());
            return $response;
        }
    }

    public function saveImageInDataBaseAction(Request $request){
        try {
            $jsonString = $request->getContent();
            $json = json_decode($jsonString);
            $imgName = $json->imgName;
            $idCase = $json->idCase;
            $roadCode = $json->roadCode;

            $imgEntity = $this->getDoctrine()->getRepository('AppBundle:Image')->createNewImage($imgName);

            $roadEntity = $this->getDoctrine()->getRepository('AppBundle:Road')->findOneBy(array('code' => $roadCode));

            if($roadEntity == null){
                $response = $this->createBadRequestResponse("No existe ese camino");
                return $response;
            }

            $imgByRouteCaseEntity = $this->getDoctrine()
                                        ->getRepository('AppBundle:ImgByRouteCase')
                                        ->createNewRecord($imgEntity->getId(), $idCase, $roadEntity->getId());

            $response = $this->createOKRequestResponse($imgByRouteCaseEntity->getId());
            return $response;

        }catch (\Exception $e){
            $response = $this->createBadRequestResponse($e->getMessage());
            return $response;
        }
    }

    public function showSenseTestTwigAction(Request $request){
        $jsonString = $request->getContent();
        $json = json_decode($jsonString);
        $teethQuantity = $json->teethQuantity;
        return $this->render(ControllerConstants::ADMIN_UPLOAD_CLTEST,array('teethQuantity' => $teethQuantity - 1));
    }

    public function registerSenseTestAction(Request $request){
        try {
            $jsonString = $request->getContent();
            $json = json_decode($jsonString);
            $description = $json->description;
            $idCase = $json->idCase;
            $teethArray = $json->teeth;

            $caseByRoadEntity = $this->getDoctrine()->getRepository('AppBundle:CaseByRoad')->createCaseByRoad($idCase,ControllerConstants::SENSE_TEST_CODE,$description);

            $this->getDoctrine()->getRepository('AppBundle:ToothByTest')->registerTeethFromAdmin($teethArray,$caseByRoadEntity);

            $response = $this->createOKRequestResponse("ok");
            return $response;
        }catch (\Exception $e){
            $response = $this->createBadRequestResponse($e->getMessage());
            return $response;
        }

    }
}