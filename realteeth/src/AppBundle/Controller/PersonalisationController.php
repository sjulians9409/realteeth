<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 22/09/17
 * Time: 03:15 PM
 */

namespace AppBundle\Controller;

use AppBundle\Constants\ControllerConstants;
use AppBundle\Entity\Level;
use AppBundle\Entity\ParametricData;
use AppBundle\Entity\PatientCase;
use AppBundle\Entity\User;
use AppBundle\Entity\UserByLevel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PersonalisationController extends Controller
{
    /**
     * This is the controller which starts the process of assigning a case
     * @param Request $request
     *
     * JSM
     */
    public function assignPatientCaseAction(Request $request){
        $patientCase= $request->query->get('caseid',null);
        $assignedCasesQuantity = $this->getEnabledAssignedCasesQuantity();

        if($assignedCasesQuantity == ControllerConstants::MAX_CASES){
            $userLevel = $this->getUserLevel();
            return $this->render(ControllerConstants::SHOW_END_GAME, array('userLevel' => $userLevel));
        }

        if($patientCase == null) {
            $patientCase = $this->assignPatientCaseFunctionality($this->getUser()->getId());
            $this->removeUserAnswersByPatientCase($this->getUser(),$patientCase);
        }else{
            $patientCase = $this->getDoctrine()->getRepository('AppBundle:PatientCase')->findOneBy(array('id'=>$patientCase));
        }

        return $this->render('case/showPatientCase.html.twig', array('patientCase'=> $patientCase,'idCase' => $patientCase->getId()));
    }

    public function getEnabledAssignedCasesQuantity(){
        return $this->getDoctrine()
                    ->getRepository('AppBundle:AssignedCases')
                    ->getEnabledAssignedCasesQuantity($this->getUser());
    }

    public function getUserLevel(){
        return $this->getDoctrine()
            ->getRepository('AppBundle:UserByLevel')
            ->getUserLevel($this->getUser());
    }

    /**
     * This function is the process necesary to assign a case
     * @param $idUser => User's id
     *
     * JSM
     */
    public function assignPatientCaseFunctionality( $idUser ){
        $userLevel = $this->getDoctrine()->getRepository('AppBundle:UserByLevel')->getUserLevel($idUser);
        if($userLevel == null || $userLevel == false){
            return false;
        }
        $potentialCases =  $this->getDoctrine()->getRepository('AppBundle:PatientCase')->getCasesByLevel($userLevel);
        if($potentialCases == false){
            return false;
        }

        shuffle($potentialCases);

        $assignedCasesIds = $this->getDoctrine()->getRepository('AppBundle:AssignedCases')->getEnabledAssignedCasesIds($this->getUser());

        $selectedCase = $potentialCases[0];
        /** @var PatientCase $p */
        foreach ($potentialCases as $p){
            if( in_array($p->getId(),$assignedCasesIds) == false ){
                $selectedCase = $p;
                break;
            }
        }
        return $selectedCase;
    }

    public function calculateStudentLevelAction(Request $request){
        $approve = $request->query->get('approve', null);
        $res = $this->setStudentLevel($approve);
        /** @var ParametricData $maxCases */
        $maxCases = $this->getDoctrine()->getRepository('AppBundle:ParametricData')->findOneBy(array('code'=>'MXCASES'));
        $actualcases = count($_SESSION['idCases']);
        //Redireccionar a un nuevo caso
        if($actualcases < $maxCases->getValue()){
            return $this->redirectToRoute('AppBundle_assign_patient_case');
        }

        //Calcular nivel despues de cantidad de casos maximos

        return $this->redirectToRoute('AppBundle_user_home',array('welcome' => true));



    }

    public function removeUserAnswersByPatientCase($idUser, $idPatientCase){
        $this->getDoctrine()->getRepository('AppBundle:UserAnswer')->removeUserAnswersByPatientCase($idUser, $idPatientCase);
    }
}