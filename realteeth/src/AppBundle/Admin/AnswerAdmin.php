<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 29/10/17
 * Time: 01:18 PM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AnswerAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id','integer')
            ->add('detail','text')
            ->add('idQuestion','entity',array(
                'class'     => 'AppBundle:Question',
                'property'  => 'detail',
                'label'     => 'Pregunta'
            ))
            ->add('idPatientCase','entity',array(
                'class'     => 'AppBundle:PatientCase',
                'property'  => 'id',
                'label'     => 'Id Caso'
            ))

        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id',null,array('label' => 'Id'));
        $listMapper->addIdentifier('detail',null,array('label' => 'Respuesta'));
        $listMapper->addIdentifier('idQuestion.detail',null,array('label' => 'Pregunta'));
        $listMapper->addIdentifier('idPatientCase.id',null,array('label' => 'Id Caso'));


    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

}