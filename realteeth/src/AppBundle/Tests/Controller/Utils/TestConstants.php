<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 12/02/18
 * Time: 05:00 PM
 */

namespace AppBundle\Tests\Controller\Utils;


class TestConstants
{
    public static $TEMPLATE_CLASS_PATH = "Symfony\Bundle\FrameworkBundle\Templating\EngineInterface";
    public static $CONTAINER_CLASS_PATH = "Symfony\Component\DependencyInjection\ContainerInterface";
    public static $RESPONSE_CLASS_PATH = "Symfony\Component\HttpFoundation\Response";
    public static $EM_PATH = "Doctrine\ORM\EntityManager";
    public static $REPOSITORY_PATH = "Doctrine\ORM\EntityRepository";
    public static $COLLECTION_PATH = "Doctrine\Common\Collections\Collection";
    public static $ROAD_ENTITY_PATH = "AppBundle\Entity\Road";
    public static $ASPECT_ENTITY_PATH = "AppBundle\Entity\Aspect";
    public static $RUBRIC_CONTROLLER_PATH = "AppBundle\Controller\RubricController";


}