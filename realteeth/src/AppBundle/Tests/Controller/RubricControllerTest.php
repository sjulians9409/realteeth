<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 12/02/18
 * Time: 11:57 AM
 */

namespace AppBundle\Tests\Controller;

use AppBundle\Controller\RubricController;
use AppBundle\Entity\Aspect;
use AppBundle\Tests\Controller\Utils\TestConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Doctrine\ORM\EntityRepository;



class RubricControllerTest extends \PHPUnit_Framework_TestCase
{
    /** @var RubricController $rubricController */
    protected $rubricController;
    private $url = "/studio/showrubric?rubric=PAIN&idCase=1";
    private $idCase = 1;
    private $roadCode = "PAIN";
    private $ajaxRequest = "http://127.0.0.1:8000/studio/registeruseranswers?jsonCriteria=[{\"idCriterionQuestion\":\"1\",\"answer\":\"Sintomático\",\"idPatientCase\":1},{\"idCriterionQuestion\":\"2\",\"answer\":\"Ausente\",\"idPatientCase\":1},{\"idCriterionQuestion\":\"4\",\"answer\":\"asd\",\"idPatientCase\":1}]";

    protected function setUp(){
        $this->rubricController = new RubricController();
    }


    public function testGetRubricByCodeShouldReturnAStringOfTheRubricTwig()
    {
        $expectedResult = "rubric/painRoadRubric.html.twig";
        $givenResult = $this->rubricController->getRubricByCode("PAIN");

        $this->assertNotNull($givenResult);
        $this->assertEquals($expectedResult,$givenResult);
    }

    public function testingShowRubricAction(){

        $this->rubricController = $this->createPartialMock(TestConstants::$RUBRIC_CONTROLLER_PATH,array('getCritireafromRoad'));

        //Mocking Aspect from method getAspectfromRoad
        $criteriaArray = array();
        $this->rubricController->expects($this->any())
                                ->method('getCritireafromRoad')
                                ->willReturn($criteriaArray);

        //Expected Response
        $response = $this->getMockBuilder(TestConstants::$RESPONSE_CLASS_PATH)
            ->getMock();

        //Mocking Templating
        $templating = $this->getMockBuilder(TestConstants::$TEMPLATE_CLASS_PATH)
                        ->getMock();
        $templating->method("renderResponse")
                    ->with(RubricController::$PAIN_ROAD_TWIG,array("idCase" => $this->idCase,'criteriaArray' => $criteriaArray))
                    ->willReturn($response);

        //Mocking Container
        $container = $this->getMockBuilder(TestConstants::$CONTAINER_CLASS_PATH)
                        ->getMock();
        $container->expects($this->once())
            ->method('get')
            ->willReturn($templating);

        //Setting Container
        $this->rubricController->setContainer($container);

        //Creqting Request
        $request = Request::create($this->url);

        //Testing Method
        $expectedResponse = $this->rubricController->showRubricAction($request);

        $this->assertEquals($response,$expectedResponse );
    }

    public function testGetCritireafromRoadShouldReturnCriteriaArray(){

        //Mocking Container
        $container = $this->getMockBuilder(TestConstants::$CONTAINER_CLASS_PATH)
            ->getMock();
        //Setting Container
        $this->rubricController->setContainer($container);

        $container->method("has")
            ->with('doctrine')
            ->willReturn(true);

        //Mocking em
        $em = $this->getMockBuilder(TestConstants::$EM_PATH)
                    ->disableOriginalConstructor()
                    ->getMock();
        $container->method('get')
                ->with('doctrine')
                ->willReturn($em);

        //Mocking repository
        $repository = $this->getMockBuilder(TestConstants::$REPOSITORY_PATH)
                        ->disableOriginalConstructor()
                        ->getMock();
        $em->method('getRepository')
            ->with('AppBundle:Road')
            ->willReturn($repository);

        //Mocking Road
        $road = $this->getMockBuilder(TestConstants::$ROAD_ENTITY_PATH)
                    ->getMock();
        $repository->method('findOneBy')
                    ->with(array('code' => $this->roadCode))
                    ->willReturn($road);

        $aspect = $this->getMockBuilder(TestConstants::$ASPECT_ENTITY_PATH)
                        ->disableOriginalConstructor()
                        ->getMock();

        $road->method('getIdAspect')
            ->willReturn($aspect);
        //Mocking collection
        $collection = $this->getMockBuilder(TestConstants::$COLLECTION_PATH)
                            ->disableOriginalConstructor()
                            ->getMock();
        $aspect->method('getCriteria')
                ->willReturn($collection);
        $criteriaArray = array();

        $collection->method('toArray')
                    ->willReturn($criteriaArray);

        $response = $this->rubricController->getCritireafromRoad($this->roadCode);



        $this->assertEquals($criteriaArray,$response);
    }


}
