<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 9/03/18
 * Time: 01:24 PM
 */

namespace AppBundle\Constants;


interface DomainConstants
{
    const COLD_TEST = "COLDT";
    const HEAT_TEST = "HEATT";
    const ELECTRIC_TEST = "ELECT";

}