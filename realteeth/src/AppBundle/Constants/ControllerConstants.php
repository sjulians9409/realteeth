<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 9/03/18
 * Time: 02:13 PM
 */

namespace AppBundle\Constants;


interface ControllerConstants
{
    //------------------- Twig paths----------------------
    const PAIN_ROAD = "case/showPainRoad.html.twig";
    const PRE_CLINICAL_REVIEW_ROAD = "case/preClinicalReview.html.twig";
    const CLINICAL_REVIEW_ROAD = "case/showClinicalReviewRoad.html.twig";
    const PRE_CLINICAL_TESTS_ROAD = "case/preClinicalTest.html.twig";
    const PERIAPICAL_TEST_ROAD = "case/showPeriapicalTest.html.twig";
    const DIAGNOSIS_RUBRIC = "rubric/diagnosisRubric.html.twig";
    const SENSE_TEST_ROAD = "case/showSenseTest.html.twig";
    const XRAY_TEST_ROAD = "case/showXRayTest.html.twig";
    const ADMIN_HONE = "admin/homeAdmin.html.twig";
    const ADMIN_CREATE_CASE = "admin/createCase.html.twig";
    const ADMIN_UPLOAD_IMAGES = "admin/images/uploadImages.html.twig";
    const ADMIN_UPLOAD_CLTEST = "admin/informative/informativeQuestionsSenseTest.html.twig";
    const SHOW_PROGRESS = "case/showPatientCasesProgress.html.twig";
    const SHOW_END_GAME = "case/showEndGame.html.twig";
    //------------------- / Twig paths----------------------

    //------------------- CODES ----------------------
    const PAIN_CODE = "PAIN";
    const PRE_CLINICAL_REVIEW_CODE = "PRECLREVIEW";
    const CLINICAL_REVIEW_CODE = "CLREVIEW";
    const PRE_CLINICAL_TESTS_CODE = "PRECLTESTS";
    const PERIAPICAL_TEST_CODE = "PERIATEST";
    const SENSE_TEST_CODE = "SENSETEST";
    const DIAG_TEST_CODE = "DIAG";
    const CLINICAL_TEST_CODE = "CLTESTS";
    const XRAY_CODE = "XRAY";
    const PROGRESS_CODE = "PROGRESS";
    const MAX_CASES = 10;
    //------------------- / CODES ----------------------

    //------------------- TOOTH STRUCTURE CODES ----------------------
    const CORONA_CLINICA = "CCLINICA";
    const RAIZ = "RAIZ";
    const TEJIDOS_SOPORTE = "TSOPORTE";
    //------------------- / TOOTH STRUCTURE CODES ----------------------

    //------------------- RUBRIC CODES ----------------------
    const MAX_LOST_POINTS = 4;
    const CONSECUTIVE_CASES = 3;
    const MAX_LEVEL = 3;
    const MIN_LEVEL = 1;
    const ID_PULPAR_DIAGNOSIS = 7;
    const ID_PERIAPICAL_DIAGNOSIS = 8;
    const MAX_FAILED_CASES = 4;


    //------------------- / RUBRIC CODES ----------------------

    //------------------- ADMIN ANALYSIS TWIGS -----------------

    const ANALYSIS_PAIN_TWIG = "admin/analysis/analysisPain.html.twig";
    const ANALYSIS_CLREVIEW_TWIG = "admin/analysis/analysisClReview.html.twig";
    const ANALYSIS_CLTESTS_TWIG = "admin/analysis/analysisClTests.html.twig";
    const ANALYSIS_XRAY_TWIG = "admin/analysis/analysisXRay.html.twig";
    const ANALYSIS_DIAGNOSIS_TWIG = "admin/analysis/analysisDiagnosis.html.twig";

    //------------------- / ADMIN ANALYSIS TWIGS -----------------

}
