<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Criterion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CriterionRepository")
 */
class Criterion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Aspect",inversedBy="criteria")
     * @ORM\JoinColumn(name="idAspect",referencedColumnName="id")
     */
    private $idAspect;

    /**
     * @var string
     *
     * @ORM\Column(name="definition", type="string", length=255)
     */
    private $definition;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxPoints", type="integer")
     */
    private $maxPoints;

    /**
     * @ORM\OneToOne(targetEntity="CriterionFeedback", mappedBy="idCriterion")
     */
    private $criterionFeedback;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set definition
     *
     * @param string $definition
     *
     * @return Criterion
     */
    public function setDefinition($definition)
    {
        $this->definition = $definition;

        return $this;
    }

    /**
     * Get definition
     *
     * @return string
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * Set maxPoints
     *
     * @param integer $maxPoints
     *
     * @return Criterion
     */
    public function setMaxPoints($maxPoints)
    {
        $this->maxPoints = $maxPoints;

        return $this;
    }

    /**
     * Get maxPoints
     *
     * @return integer
     */
    public function getMaxPoints()
    {
        return $this->maxPoints;
    }

    /**
     * Set idAspect
     *
     * @param \AppBundle\Entity\Aspect $idAspect
     *
     * @return Criterion
     */
    public function setIdAspect(\AppBundle\Entity\Aspect $idAspect = null)
    {
        $this->idAspect = $idAspect;

        return $this;
    }

    /**
     * Get idAspect
     *
     * @return \AppBundle\Entity\Aspect
     */
    public function getIdAspect()
    {
        return $this->idAspect;
    }

    /**
     * Set criterionFeedback
     *
     * @param \AppBundle\Entity\CriterionFeedback $criterionFeedback
     *
     * @return Criterion
     */
    public function setCriterionFeedback(\AppBundle\Entity\CriterionFeedback $criterionFeedback = null)
    {
        $this->criterionFeedback = $criterionFeedback;

        return $this;
    }

    /**
     * Get criterionFeedback
     *
     * @return \AppBundle\Entity\CriterionFeedback
     */
    public function getCriterionFeedback()
    {
        return $this->criterionFeedback;
    }
}
