<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAnswer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserAnswerRepository")
 */
class UserAnswer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CriterionQuestion")
     * @ORM\JoinColumn(name="idCriterionQuestion", referencedColumnName="id")
     */
    private $idCriterionQuestion;

    /**
     * @ORM\ManyToOne(targetEntity="PatientCase")
     * @ORM\JoinColumn(name="idPatientCase", referencedColumnName="id")
     */
    private $idPatientCase;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="string", length=255)
     */
    private $detail;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detail
     *
     * @param string $detail
     *
     * @return UserAnswer
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set idCriterionQuestion
     *
     * @param \AppBundle\Entity\CriterionQuestion $idCriterionQuestion
     *
     * @return UserAnswer
     */
    public function setIdCriterionQuestion(\AppBundle\Entity\CriterionQuestion $idCriterionQuestion = null)
    {
        $this->idCriterionQuestion = $idCriterionQuestion;

        return $this;
    }

    /**
     * Get idCriterionQuestion
     *
     * @return \AppBundle\Entity\CriterionQuestion
     */
    public function getIdCriterionQuestion()
    {
        return $this->idCriterionQuestion;
    }

    /**
     * Set idPatientCase
     *
     * @param \AppBundle\Entity\PatientCase $idPatientCase
     *
     * @return UserAnswer
     */
    public function setIdPatientCase(\AppBundle\Entity\PatientCase $idPatientCase = null)
    {
        $this->idPatientCase = $idPatientCase;

        return $this;
    }

    /**
     * Get idPatientCase
     *
     * @return \AppBundle\Entity\PatientCase
     */
    public function getIdPatientCase()
    {
        return $this->idPatientCase;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return UserAnswer
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
