<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImgByRouteCase
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ImgByRouteCaseRepository")
 */
class ImgByRouteCase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="idImg", referencedColumnName="id")
     */
    private $idImg;

    /**
     * @ORM\ManyToOne(targetEntity="PatientCase")
     * @ORM\JoinColumn(name="idPatientCase", referencedColumnName="id")
     */
    private $idPatientCase;

    /**
     * @ORM\ManyToOne(targetEntity="Road")
     * @ORM\JoinColumn(name="idRoad", referencedColumnName="id")
     */
    private $idRoad;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idImg
     *
     * @param \AppBundle\Entity\Image $idImg
     *
     * @return ImgByRouteCase
     */
    public function setIdImg(\AppBundle\Entity\Image $idImg = null)
    {
        $this->idImg = $idImg;

        return $this;
    }

    /**
     * Get idImg
     *
     * @return \AppBundle\Entity\Image
     */
    public function getIdImg()
    {
        return $this->idImg;
    }

    /**
     * Set idPatientCase
     *
     * @param \AppBundle\Entity\PatientCase $idPatientCase
     *
     * @return ImgByRouteCase
     */
    public function setIdPatientCase(\AppBundle\Entity\PatientCase $idPatientCase = null)
    {
        $this->idPatientCase = $idPatientCase;

        return $this;
    }

    /**
     * Get idPatientCase
     *
     * @return \AppBundle\Entity\PatientCase
     */
    public function getIdPatientCase()
    {
        return $this->idPatientCase;
    }

    /**
     * Set idRoad
     *
     * @param \AppBundle\Entity\Road $idRoad
     *
     * @return ImgByRouteCase
     */
    public function setIdRoad(\AppBundle\Entity\Road $idRoad = null)
    {
        $this->idRoad = $idRoad;

        return $this;
    }

    /**
     * Get idRoad
     *
     * @return \AppBundle\Entity\Road
     */
    public function getIdRoad()
    {
        return $this->idRoad;
    }
}
