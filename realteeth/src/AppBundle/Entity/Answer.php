<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AnswerRepository")
 */
class Answer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="idQuestion", referencedColumnName="id")
     */
    private $idQuestion;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="string", length=550)
     */
    private $detail;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PatientCase")
     * @ORM\JoinColumn(name="idPatientCase", referencedColumnName="id")
     */
    private $idPatientCase;

    /**
     * @ORM\OneToMany(targetEntity="AnswerByImage", mappedBy="idAnswer")
    */
    private $answerByImages;
   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detail
     *
     * @param string $detail
     *
     * @return Answer
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set idQuestion
     *
     * @param \AppBundle\Entity\Question $idQuestion
     *
     * @return Answer
     */
    public function setIdQuestion(\AppBundle\Entity\Question $idQuestion = null)
    {
        $this->idQuestion = $idQuestion;

        return $this;
    }

    /**
     * Get idQuestion
     *
     * @return \AppBundle\Entity\Question
     */
    public function getIdQuestion()
    {
        return $this->idQuestion;
    }

    /**
     * Set idPatientCase
     *
     * @param \AppBundle\Entity\PatientCase $idPatientCase
     *
     * @return Answer
     */
    public function setIdPatientCase(\AppBundle\Entity\PatientCase $idPatientCase = null)
    {
        $this->idPatientCase = $idPatientCase;

        return $this;
    }

    /**
     * Get idPatientCase
     *
     * @return \AppBundle\Entity\PatientCase
     */
    public function getIdPatientCase()
    {
        return $this->idPatientCase;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answerByImages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add answerByImage
     *
     * @param \AppBundle\Entity\AnswerByImage $answerByImage
     *
     * @return Answer
     */
    public function addAnswerByImage(\AppBundle\Entity\AnswerByImage $answerByImage)
    {
        $this->answerByImages[] = $answerByImage;

        return $this;
    }

    /**
     * Remove answerByImage
     *
     * @param \AppBundle\Entity\AnswerByImage $answerByImage
     */
    public function removeAnswerByImage(\AppBundle\Entity\AnswerByImage $answerByImage)
    {
        $this->answerByImages->removeElement($answerByImage);
    }

    /**
     * Get answerByImages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswerByImages()
    {
        return $this->answerByImages;
    }
}
