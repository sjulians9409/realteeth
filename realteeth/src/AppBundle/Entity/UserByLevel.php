<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserByLevel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserByLevelRepository")
 */
class UserByLevel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Level")
     * @ORM\JoinColumn(name="idLevel", referencedColumnName="id")
     */
    private $idLevel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="levelDate", type="datetime")
     */
    private $levelDate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set levelDate
     *
     * @param \DateTime $levelDate
     *
     * @return UserByLevel
     */
    public function setLevelDate($levelDate)
    {
        $this->levelDate = $levelDate;

        return $this;
    }

    /**
     * Get levelDate
     *
     * @return \DateTime
     */
    public function getLevelDate()
    {
        return $this->levelDate;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return UserByLevel
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idLevel
     *
     * @param \AppBundle\Entity\Level $idLevel
     *
     * @return UserByLevel
     */
    public function setIdLevel(\AppBundle\Entity\Level $idLevel = null)
    {
        $this->idLevel = $idLevel;

        return $this;
    }

    /**
     * Get idLevel
     *
     * @return \AppBundle\Entity\Level
     */
    public function getIdLevel()
    {
        return $this->idLevel;
    }
}
