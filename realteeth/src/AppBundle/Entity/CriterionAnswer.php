<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CriterionAnswer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CriterionAnswerRepository")
 */
class CriterionAnswer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CriterionQuestion")
     * @ORM\JoinColumn(name="idCriterionQuestion",referencedColumnName="id")
     */
    private $idCriterionQuestion;

    /**
     * @ORM\ManyToOne(targetEntity="PatientCase")
     * @ORM\JoinColumn(name="idPatientCase",referencedColumnName="id")
     */
    private $idPatientCase;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="string", length=255)
     */
    private $detail;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detail
     *
     * @param string $detail
     *
     * @return CriterionAnswer
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set idCriterionQuestion
     *
     * @param \AppBundle\Entity\CriterionQuestion $idCriterionQuestion
     *
     * @return CriterionAnswer
     */
    public function setIdCriterionQuestion(\AppBundle\Entity\CriterionQuestion $idCriterionQuestion = null)
    {
        $this->idCriterionQuestion = $idCriterionQuestion;

        return $this;
    }

    /**
     * Get idCriterionQuestion
     *
     * @return \AppBundle\Entity\CriterionQuestion
     */
    public function getIdCriterionQuestion()
    {
        return $this->idCriterionQuestion;
    }

    /**
     * Set idPatientCase
     *
     * @param \AppBundle\Entity\PatientCase $idPatientCase
     *
     * @return CriterionAnswer
     */
    public function setIdPatientCase(\AppBundle\Entity\PatientCase $idPatientCase = null)
    {
        $this->idPatientCase = $idPatientCase;

        return $this;
    }

    /**
     * Get idPatientCase
     *
     * @return \AppBundle\Entity\PatientCase
     */
    public function getIdPatientCase()
    {
        return $this->idPatientCase;
    }
}
