<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CaseByRoad
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CaseByRoadRepository")
 */
class CaseByRoad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PatientCase")
     * @ORM\JoinColumn(name="idPatientCase",referencedColumnName="id")
     */
    private $idPatientCase;

    /**
     * @ORM\ManyToOne(targetEntity="Road")
     * @ORM\JoinColumn(name="idRoad",referencedColumnName="id")
     */
    private $idRoad;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="string", length=255)
     */
    private $detail;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detail
     *
     * @param string $detail
     *
     * @return CaseByRoad
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set idPatientCase
     *
     * @param \AppBundle\Entity\PatientCase $idPatientCase
     *
     * @return CaseByRoad
     */
    public function setIdPatientCase(\AppBundle\Entity\PatientCase $idPatientCase = null)
    {
        $this->idPatientCase = $idPatientCase;

        return $this;
    }

    /**
     * Get idPatientCase
     *
     * @return \AppBundle\Entity\PatientCase
     */
    public function getIdPatientCase()
    {
        return $this->idPatientCase;
    }

    /**
     * Set idRoad
     *
     * @param \AppBundle\Entity\Road $idRoad
     *
     * @return CaseByRoad
     */
    public function setIdRoad(\AppBundle\Entity\Road $idRoad = null)
    {
        $this->idRoad = $idRoad;

        return $this;
    }

    /**
     * Get idRoad
     *
     * @return \AppBundle\Entity\Road
     */
    public function getIdRoad()
    {
        return $this->idRoad;
    }
}
