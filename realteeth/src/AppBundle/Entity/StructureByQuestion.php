<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StructureByQuestion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\StructureByQuestionRepository")
 */
class StructureByQuestion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ToothStructure")
     * @ORM\JoinColumn(name="idStructure", referencedColumnName="id")
     */
    private $idStructure;

    /**
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="idQuestion", referencedColumnName="id")
     */
    private $idQuestion;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idStructure
     *
     * @param \AppBundle\Entity\ToothStructure $idStructure
     *
     * @return StructureByQuestion
     */
    public function setIdStructure(\AppBundle\Entity\ToothStructure $idStructure = null)
    {
        $this->idStructure = $idStructure;

        return $this;
    }

    /**
     * Get idStructure
     *
     * @return \AppBundle\Entity\ToothStructure
     */
    public function getIdStructure()
    {
        return $this->idStructure;
    }

    /**
     * Set idQuestion
     *
     * @param \AppBundle\Entity\Question $idQuestion
     *
     * @return StructureByQuestion
     */
    public function setIdQuestion(\AppBundle\Entity\Question $idQuestion = null)
    {
        $this->idQuestion = $idQuestion;

        return $this;
    }

    /**
     * Get idQuestion
     *
     * @return \AppBundle\Entity\Question
     */
    public function getIdQuestion()
    {
        return $this->idQuestion;
    }
}
