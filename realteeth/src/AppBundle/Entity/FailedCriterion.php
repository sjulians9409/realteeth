<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FailedCriterion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FailedCriterionRepository")
 */
class FailedCriterion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Criterion")
     * @ORM\JoinColumn(name="idCriterion",referencedColumnName="id")
     */
    private $idCriterion;

    /**
     * @ORM\ManyToOne(targetEntity="PatientCase")
     * @ORM\JoinColumn(name="idPatientCase", referencedColumnName="id")
     */
    private $idPatientCase;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="User")
     */
    private $idUser;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCriterion
     *
     * @param \AppBundle\Entity\Criterion $idCriterion
     *
     * @return FailedCriterion
     */
    public function setIdCriterion(\AppBundle\Entity\Criterion $idCriterion = null)
    {
        $this->idCriterion = $idCriterion;

        return $this;
    }

    /**
     * Get idCriterion
     *
     * @return \AppBundle\Entity\Criterion
     */
    public function getIdCriterion()
    {
        return $this->idCriterion;
    }

    /**
     * Set idPatientCase
     *
     * @param \AppBundle\Entity\PatientCase $idPatientCase
     *
     * @return FailedCriterion
     */
    public function setIdPatientCase(\AppBundle\Entity\PatientCase $idPatientCase = null)
    {
        $this->idPatientCase = $idPatientCase;

        return $this;
    }

    /**
     * Get idPatientCase
     *
     * @return \AppBundle\Entity\PatientCase
     */
    public function getIdPatientCase()
    {
        return $this->idPatientCase;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return FailedCriterion
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
