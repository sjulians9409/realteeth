<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserByCourse
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserByCourseRepository")
 */
class UserByCourse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumn(name="idCourse", referencedColumnName="id")
     */
    private $idCourse;

    /**
     * @var boolean
     *
     * @ORM\Column(name="professor", type="boolean")
     */
    private $professor;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set professor
     *
     * @param boolean $professor
     *
     * @return UserByCourse
     */
    public function setProfessor($professor)
    {
        $this->professor = $professor;

        return $this;
    }

    /**
     * Get professor
     *
     * @return boolean
     */
    public function getProfessor()
    {
        return $this->professor;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return UserByCourse
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idCourse
     *
     * @param \AppBundle\Entity\Course $idCourse
     *
     * @return UserByCourse
     */
    public function setIdCourse(\AppBundle\Entity\Course $idCourse = null)
    {
        $this->idCourse = $idCourse;

        return $this;
    }

    /**
     * Get idCourse
     *
     * @return \AppBundle\Entity\Course
     */
    public function getIdCourse()
    {
        return $this->idCourse;
    }
}
