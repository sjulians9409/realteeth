<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="names", type="string", length=255)
     */
    private $names;

    /**
     * @var string
     *
     * @ORM\Column(name="surNames", type="string", length=255)
     */
    private $surNames;

    /**
     * @var integer
     *
     * @ORM\Column(name="semester", type="integer")
     */
    private $semester;

    /**
     * @var integer
     *
     * @ORM\Column(name="consecutiveCases", type="integer", options={"default"=0})
     */
    private $consecutiveCases;

    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    

   

    /**
     * Set names
     *
     * @param string $names
     *
     * @return User
     */
    public function setNames($names)
    {
        $this->names = $names;

        return $this;
    }

    /**
     * Get names
     *
     * @return string
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * Set surNames
     *
     * @param string $surNames
     *
     * @return User
     */
    public function setSurNames($surNames)
    {
        $this->surNames = $surNames;

        return $this;
    }

    /**
     * Get surNames
     *
     * @return string
     */
    public function getSurNames()
    {
        return $this->surNames;
    }

    /**
     * Set idGender
     *
     * @param \AppBundle\Entity\Gender $idGender
     *
     * @return User
     */
    public function setIdGender(\AppBundle\Entity\Gender $idGender = null)
    {
        $this->idGender = $idGender;

        return $this;
    }

    /**
     * Get idGender
     *
     * @return \AppBundle\Entity\Gender
     */
    public function getIdGender()
    {
        return $this->idGender;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return User
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set consecutiveCases
     *
     * @param integer $consecutiveCases
     *
     * @return User
     */
    public function setConsecutiveCases($consecutiveCases)
    {
        $this->consecutiveCases = $consecutiveCases;

        return $this;
    }

    /**
     * Get consecutiveCases
     *
     * @return integer
     */
    public function getConsecutiveCases()
    {
        return $this->consecutiveCases;
    }
}
