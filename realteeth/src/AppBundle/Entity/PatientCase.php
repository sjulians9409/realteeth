<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PatientCase
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PatientCaseRepository")
 */
class PatientCase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Level")
     * @ORM\JoinColumn(name="idLevel", referencedColumnName="id")
     */
    private $idLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="illnessHistory", type="string", length=550)
     */
    private $illnessHistory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set illnessHistory
     *
     * @param string $illnessHistory
     *
     * @return PatientCase
     */
    public function setIllnessHistory($illnessHistory)
    {
        $this->illnessHistory = $illnessHistory;

        return $this;
    }

    /**
     * Get illnessHistory
     *
     * @return string
     */
    public function getIllnessHistory()
    {
        return $this->illnessHistory;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return PatientCase
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set idLevel
     *
     * @param \AppBundle\Entity\Level $idLevel
     *
     * @return PatientCase
     */
    public function setIdLevel(\AppBundle\Entity\Level $idLevel = null)
    {
        $this->idLevel = $idLevel;

        return $this;
    }

    /**
     * Get idLevel
     *
     * @return \AppBundle\Entity\Level
     */
    public function getIdLevel()
    {
        return $this->idLevel;
    }
}
