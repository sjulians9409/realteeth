<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\QuestionRepository")
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Road")
     * @ORM\JoinColumn(name="idRoad",referencedColumnName="id")
     */
    private $idRoad;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="string", length=255)
     */
    private $detail;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detail
     *
     * @param string $detail
     *
     * @return Question
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set idRoad
     *
     * @param \AppBundle\Entity\Road $idRoad
     *
     * @return Question
     */
    public function setIdRoad(\AppBundle\Entity\Road $idRoad = null)
    {
        $this->idRoad = $idRoad;

        return $this;
    }

    /**
     * Get idRoad
     *
     * @return \AppBundle\Entity\Road
     */
    public function getIdRoad()
    {
        return $this->idRoad;
    }
}
