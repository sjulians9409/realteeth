<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CriterionQuestion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CriterionQuestionRepository")
 */
class CriterionQuestion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Criterion")
     * @ORM\JoinColumn(name="idCriterion",referencedColumnName="id")
     */
    private $idCriterion;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="string", length=255)
     */
    private $detail;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detail
     *
     * @param string $detail
     *
     * @return CriterionQuestion
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set idCriterion
     *
     * @param \AppBundle\Entity\Criterion $idCriterion
     *
     * @return CriterionQuestion
     */
    public function setIdCriterion(\AppBundle\Entity\Criterion $idCriterion = null)
    {
        $this->idCriterion = $idCriterion;

        return $this;
    }

    /**
     * Get idCriterion
     *
     * @return \AppBundle\Entity\Criterion
     */
    public function getIdCriterion()
    {
        return $this->idCriterion;
    }
}
