<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Aspect
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AspectRepository")
 */
class Aspect
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="Road", inversedBy="idAspect")
     * @ORM\JoinColumn(name="idRoad", referencedColumnName="id")
     */
    private $idRoad;

    /**
     * @ORM\OneToMany(targetEntity="Criterion", mappedBy="idAspect")
     */
    private $criteria;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Aspect
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set idRoad
     *
     * @param \AppBundle\Entity\Road $idRoad
     *
     * @return Aspect
     */
    public function setIdRoad(\AppBundle\Entity\Road $idRoad = null)
    {
        $this->idRoad = $idRoad;

        return $this;
    }

    /**
     * Get idRoad
     *
     * @return \AppBundle\Entity\Road
     */
    public function getIdRoad()
    {
        return $this->idRoad;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->criteria = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add criterion
     *
     * @param \AppBundle\Entity\Criterion $criterion
     *
     * @return Aspect
     */
    public function addCriterium(\AppBundle\Entity\Criterion $criterion)
    {
        $this->criteria[] = $criterion;

        return $this;
    }

    /**
     * Remove criterion
     *
     * @param \AppBundle\Entity\Criterion $criterion
     */
    public function removeCriterium(\AppBundle\Entity\Criterion $criterion)
    {
        $this->criteria->removeElement($criterion);
    }

    /**
     * Get criteria
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCriteria()
    {
        return $this->criteria;
    }
}
