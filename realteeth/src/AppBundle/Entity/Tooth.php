<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tooth
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ToothRepository")
 */
class Tooth
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseByRoad")
     * @ORM\JoinColumn(name="idCaseByRoad", referencedColumnName="id")
     */
    private $idCaseByRoad;

    /**
     * @var integer
     *
     * @ORM\Column(name="toothNumber", type="integer", nullable=true )
     */
    private $toothNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="referred", type="boolean")
     */
    private $referred;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set toothNumber
     *
     * @param integer $toothNumber
     *
     * @return Tooth
     */
    public function setToothNumber($toothNumber)
    {
        $this->toothNumber = $toothNumber;

        return $this;
    }

    /**
     * Get toothNumber
     *
     * @return integer
     */
    public function getToothNumber()
    {
        return $this->toothNumber;
    }

    /**
     * Set referred
     *
     * @param boolean $referred
     *
     * @return Tooth
     */
    public function setReferred($referred)
    {
        $this->referred = $referred;

        return $this;
    }

    /**
     * Get referred
     *
     * @return boolean
     */
    public function getReferred()
    {
        return $this->referred;
    }

    /**
     * Set idCaseByRoad
     *
     * @param \AppBundle\Entity\CaseByRoad $idCaseByRoad
     *
     * @return Tooth
     */
    public function setIdCaseByRoad(\AppBundle\Entity\CaseByRoad $idCaseByRoad = null)
    {
        $this->idCaseByRoad = $idCaseByRoad;

        return $this;
    }

    /**
     * Get idCaseByRoad
     *
     * @return \AppBundle\Entity\CaseByRoad
     */
    public function getIdCaseByRoad()
    {
        return $this->idCaseByRoad;
    }
}
