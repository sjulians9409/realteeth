<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GainedPoints
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GainedPointsRepository")
 */
class GainedPoints
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Criterion")
     * @ORM\JoinColumn(name="idCriterion", referencedColumnName="id")
     */
    private $idCriterion;

    /**
     * @ORM\ManyToOne(targetEntity="PatientCase")
     * @ORM\JoinColumn(name="idPatientCase", referencedColumnName="id")
     */
    private $idPatientCase;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="pointsGained", type="integer")
     */
    private $pointsGained;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pointsGained
     *
     * @param integer $pointsGained
     *
     * @return GainedPoints
     */
    public function setPointsGained($pointsGained)
    {
        $this->pointsGained = $pointsGained;

        return $this;
    }

    /**
     * Get pointsGained
     *
     * @return integer
     */
    public function getPointsGained()
    {
        return $this->pointsGained;
    }

    /**
     * Set idCriterion
     *
     * @param \AppBundle\Entity\Criterion $idCriterion
     *
     * @return GainedPoints
     */
    public function setIdCriterion(\AppBundle\Entity\Criterion $idCriterion = null)
    {
        $this->idCriterion = $idCriterion;

        return $this;
    }

    /**
     * Get idCriterion
     *
     * @return \AppBundle\Entity\Criterion
     */
    public function getIdCriterion()
    {
        return $this->idCriterion;
    }

    /**
     * Set idPatientCase
     *
     * @param \AppBundle\Entity\PatientCase $idPatientCase
     *
     * @return GainedPoints
     */
    public function setIdPatientCase(\AppBundle\Entity\PatientCase $idPatientCase = null)
    {
        $this->idPatientCase = $idPatientCase;

        return $this;
    }

    /**
     * Get idPatientCase
     *
     * @return \AppBundle\Entity\PatientCase
     */
    public function getIdPatientCase()
    {
        return $this->idPatientCase;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return GainedPoints
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
