<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AssignedCases
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AssignedCasesRepository")
 */
class AssignedCases
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PatientCase")
     * @ORM\JoinColumn(name="idPatientCase",referencedColumnName="id")
     */
    private $idPatientCase;

    /**
     * @var boolean
     *
     * @ORM\Column(name="succeed", type="boolean")
     */
    private $succeed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

  

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set succeed
     *
     * @param boolean $succeed
     *
     * @return AssignedCases
     */
    public function setSucceed($succeed)
    {
        $this->succeed = $succeed;

        return $this;
    }

    /**
     * Get succeed
     *
     * @return boolean
     */
    public function getSucceed()
    {
        return $this->succeed;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return AssignedCases
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return AssignedCases
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idPatientCase
     *
     * @param \AppBundle\Entity\PatientCase $idPatientCase
     *
     * @return AssignedCases
     */
    public function setIdPatientCase(\AppBundle\Entity\PatientCase $idPatientCase = null)
    {
        $this->idPatientCase = $idPatientCase;

        return $this;
    }

    /**
     * Get idPatientCase
     *
     * @return \AppBundle\Entity\PatientCase
     */
    public function getIdPatientCase()
    {
        return $this->idPatientCase;
    }
}
