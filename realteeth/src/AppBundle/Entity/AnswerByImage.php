<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnswerByImage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AnswerByImageRepository")
 */
class AnswerByImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Answer",inversedBy="answerByImages")
     * @ORM\JoinColumn(name="idAnswer", referencedColumnName="id")
     */
    private $idAnswer;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="idImage", referencedColumnName="id")
     */
    private $idImage;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAnswer
     *
     * @param \AppBundle\Entity\Answer $idAnswer
     *
     * @return AnswerByImage
     */
    public function setIdAnswer(\AppBundle\Entity\Answer $idAnswer = null)
    {
        $this->idAnswer = $idAnswer;

        return $this;
    }

    /**
     * Get idAnswer
     *
     * @return \AppBundle\Entity\Answer
     */
    public function getIdAnswer()
    {
        return $this->idAnswer;
    }

    /**
     * Set idImage
     *
     * @param \AppBundle\Entity\Image $idImage
     *
     * @return AnswerByImage
     */
    public function setIdImage(\AppBundle\Entity\Image $idImage = null)
    {
        $this->idImage = $idImage;

        return $this;
    }

    /**
     * Get idImage
     *
     * @return \AppBundle\Entity\Image
     */
    public function getIdImage()
    {
        return $this->idImage;
    }
}
