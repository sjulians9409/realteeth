<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 9/03/18
 * Time: 01:12 PM
 */

namespace AppBundle\Domain;


use AppBundle\Constants\DomainConstants;

class DTooth
{
    private $toothNumber;
    private $referred;
    private $coldResult;
    private $heatResult;
    private $electricResult;

    public function setTestResult($detail,$code){
        if($code == DomainConstants::COLD_TEST){
            $this->coldResult = $detail;
        }elseif ($code == DomainConstants::HEAT_TEST){
            $this->heatResult = $detail;
        }elseif ($code == DomainConstants::ELECTRIC_TEST){
            $this->electricResult = $detail;
        }else{
            return false;
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getToothNumber()
    {
        return $this->toothNumber;
    }

    /**
     * @param mixed $toothNumber
     */
    public function setToothNumber($toothNumber)
    {
        $this->toothNumber = $toothNumber;
    }

    /**
     * @return mixed
     */
    public function getReferred()
    {
        return $this->referred;
    }

    /**
     * @param mixed $referred
     */
    public function setReferred($referred)
    {
        $this->referred = $referred;
    }

    /**
     * @return mixed
     */
    public function getColdResult()
    {
        return $this->coldResult;
    }

    /**
     * @param mixed $coldResult
     */
    public function setColdResult($coldResult)
    {
        $this->coldResult = $coldResult;
    }

    /**
     * @return mixed
     */
    public function getHeatResult()
    {
        return $this->heatResult;
    }

    /**
     * @param mixed $heatResult
     */
    public function setHeatResult($heatResult)
    {
        $this->heatResult = $heatResult;
    }

    /**
     * @return mixed
     */
    public function getElectricResult()
    {
        return $this->electricResult;
    }

    /**
     * @param mixed $electricResult
     */
    public function setElectricResult($electricResult)
    {
        $this->electricResult = $electricResult;
    }


}