<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FOS\UserBundle\Util\LegacyFormHelper;

class RegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('names','text',array(
                'label'             => 'Nombre',
            ))
            ->add('surNames','text',array(
                'label'             => 'Apellido'
            ))
            /*->add('idGender','entity',array(
                'class'             => 'AppBundle:Gender',
                'choice_label'      => 'detail',
                'expanded'          => true,
                'label'             => 'Genero',
            ))*/
            ->add('email','text', array(
                'label'             => 'Email',
            ))
            ->add('username','hidden',array(
                'data'              => 'username',
            ))
            ->add('plainPassword','password',array(
                'label'             =>'Contraseña',
            ))
            ->add('semester','choice',array(
                'choices'           => array(
                                            '5' => 5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,'10'=>10
                                        ),
                'choices_as_values' => true,
                'label'             => 'Semestre',
            ))
            ->add('course','entity',array(
                'mapped'            => false,
                'class'             => 'AppBundle:Course',
                'query_builder'     => function(EntityRepository $er){
                    return $er->createQueryBuilder('c')
                            ->where('c.enable = true')
                            ->orderBy('c.courseNumber','ASC');
                },
                'choice_label'      => 'courseNumber',
                'label'             => 'Curso',
            ))
            ->add('submit','submit',array(
                'label'             => 'Registrar'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    /*public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }*/

    public function getParent()
    {
        return 'fos_user_registration';

    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
