function getRubricAnswers(idCase){
    var criteriaArray = new Array();
    getRubricAnswersFromToothQuestions(idCase,criteriaArray);
    getRubricAnswerFromGeneralQuestions(idCase,criteriaArray);
    /*if(criteriaArray.length == 0){
        return false
    }*/
    var jsonCriteria = JSON.stringify(criteriaArray);

    return jsonCriteria;
}

function getRubricAnswersFromToothQuestions(idCase,criteriaArray){
    var elements =  $(".dev_tooth_answer");
    for( var i = 0; i < elements.length ; i++){
        if( (elements[i].type == "radio" || elements[i].type == "checkbox") && elements[i].checked == true) {
            if (elements[i].getAttribute("data-type") == "boolean" && elements[i].value == "no") {
                continue;
            }
            if(elements[i].value == "ninguna"){
                continue;
            }
            var criterionAnswer = new Object();
            var toothIndex = elements[i].getAttribute("data-tooth");
            criterionAnswer.idCriterionQuestion = elements[i].getAttribute("data-question");
            criterionAnswer.answer = getToothNumberByIndex(toothIndex);
            criterionAnswer.idPatientCase = idCase;
            criteriaArray.push(criterionAnswer);
        }
    }
    return true;
}

function getRubricAnswerFromGeneralQuestions(idcase,criteriaArray){
    var elements = $(".dev_general_answer");
    for(var i = 0 ; i < elements.length ; i++){
        var answer ="";
        if(elements[i].type == "radio" && elements[i].checked == true){
            answer = elements[i].value;
        }else if(elements[i].type != "radio"){
            answer = elements[i].value;
        }
        if(answer != "") {
            var criterionAnswer = new Object();
            criterionAnswer.idCriterionQuestion = elements[i].getAttribute("data-question");
            criterionAnswer.answer = answer;
            criterionAnswer.idPatientCase = idcase;
            criteriaArray.push(criterionAnswer);
        }
    }
    return true;
}

function getToothNumberByIndex(i){
    return $('input[data-tooth-index="' + i + '"]').val()
}

function showNoAnswersSWAL(){
    swal({
        type:'warning',
        title:'No tienes respuestas',
        text: 'Recuerda solucionar las preguntas'
    });
}

function registerRubricAnswers(url,jsonCriteria,resolve,redirection){
    $.ajax({
        url: url,
        method: 'GET',
        data:{"jsonCriteria": jsonCriteria},
        dataType: 'json',
        success: function (result) {
            //console.log(result);
            if(result == false){
                swal({
                    title: "Ooops!",
                    text: "No tenemos respuesta en esta pregunta",
                    type: "error",
                });
            }
            else{
                console.log(result);
                resolve();
                window.location.replace(redirection);
                //$(".dev_answer").html(result.detail);
            }
            //$(".dev_mensaje").html("Se Congeló el usuario");


        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            console.log(status);
            console.log(error);
            alert(err.Message);

        },
        complete: function(xhr,status){
            console.log(status);
            //$(".dev_congelar").fadeOut('slow');
        }
    });
}

function noAutoScrollOnCarousel(){
    $(".carousel").carousel({
        interval:false
    });
}